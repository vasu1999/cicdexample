FROM docker:stable


RUN apk add --update --no-cache coreutils bash git curl python3 python3-dev py3-pip jq \
                                ca-certificates openssl-dev make g++ gnupg libgcc libstdc++ \
                                gcc libc-dev linux-headers
